#include "Animal.h"

void Cat::Voice() const
{
	std::cout << "Mew-mew, I'm a Cat!\n";
}

Cat::~Cat()
{
	std::cout << "Purr-purr, goodbye!\n";
}

void Dog::Voice() const
{
	std::cout << "Woof-woof!, I'm a Dog!\n";
}

Dog::~Dog()
{
	std::cout << "Rraw-rraw, good boy!\n";
}

void Penguin::Voice() const
{
	std::cout << "Kwak-kwak, I hate Windows!\n";
}

Penguin::~Penguin()
{
	std::cout << "Bwek-bwek, freeware boy!\n";
}

void Programmer::Voice() const
{
	std::cout << "Click-click, I love virtual destructors!\n";
}

Programmer::~Programmer()
{
	std::cout << "Clap-clap, everything works!\n";
}