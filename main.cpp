#pragma once
#include <iostream>
#include "Animal.h"

/***********************************************
A little showcase of virtual function overriding
Animal is an abstract (interface) class. Cat and  
others where derived from it, overriding Voice()
***********************************************/

int main()
{
	Animal* OfficeZoo[] { new Cat, new Dog, new Penguin, new Programmer };
	for (auto a : OfficeZoo)
		a->Voice(), delete a; // also checking virtual destructor

	return 0;
}