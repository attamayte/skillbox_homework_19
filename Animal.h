#pragma once
#include <iostream>

class Animal
{
public:
	Animal() {}
	virtual ~Animal() {}
	
	virtual void Voice() const = 0;
};

class Cat : public Animal
{
public:
	Cat() : Animal() {}
	~Cat();

	void Voice() const override;
};

class Dog : public Animal
{
public:
	Dog() : Animal() {}
	~Dog();

	void Voice() const override;
};

class Penguin : public Animal
{
public:
	Penguin() : Animal() {}
	~Penguin();

	void Voice() const override;
};

class Programmer : public Animal
{
public:
	Programmer() : Animal() {}
	~Programmer();

	void Voice() const override;
};